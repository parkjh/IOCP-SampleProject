#pragma once
#include "Noncopyable.h"

/*
 * Singleton ���ø�
 */
template<typename T>
class TSingleton: public CNoncopyable
{
protected:
	static T* m_pInstance;

public:
	TSingleton(){}
	virtual ~TSingleton(){}

	static T& GetInstance()
	{
		if (!m_pInstance)
			m_pInstance = new T;

		return *m_pInstance;
	}

	static void Release()
	{
		if (m_pInstance)
		{
			delete m_pInstance;
			m_pInstance = NULL;
		}
	}
};

template<typename T> T* TSingleton<T>::m_pInstance = NULL;