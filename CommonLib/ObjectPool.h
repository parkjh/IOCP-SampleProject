#pragma once
#include "Common.h"
#include "Noncopyable.h"
#include "ThreadSync.h"
#include <atomic>

#include <thread>
#include <typeinfo>

#define DEFAULT_CHUNK_SIZE 10
#define MAX_POOL_SIZE 3000

template <typename T>
class CObjectPool
{
	typedef std::queue<shared_ptr<T>> QueueT;

private:	
	CSyncObj m_lock; // 동기화 객체
	QueueT m_qFreeList; // 사용 가능한 목록
	size_t m_szChunk;   // 오브젝트 생성 단위
	size_t m_szMaxPool; // 생성 가능한 최대 크기
	size_t m_createdSize;

public:
	// chunk size가 0일 경우 Assert 처리
	// 객체 할당 실패시 bad_alloc
	CObjectPool(size_t szChunk = DEFAULT_CHUNK_SIZE)
		throw(std::bad_alloc);
	virtual ~CObjectPool();

	void InitializePool(size_t szPool);
	shared_ptr<T> AquireObject();
	void ReleaseObject(shared_ptr<T> obj);
	size_t GetSize();
	void ClearAllObject();

	size_t GetCreateSize() { return m_createdSize; }

private:
	void ExtendPool();
	void AllocObject();
	
};


template <typename T>
CObjectPool<T>::CObjectPool(size_t szChunk = DEFAULT_CHUNK_SIZE)
throw(std::bad_alloc)
: m_szChunk(szChunk),
  m_szMaxPool(MAX_POOL_SIZE),
  m_createdSize(0)

{
	CHECK( 0 < szChunk )
		<< "chunk size must be positive";

	m_szChunk = szChunk;
}

template <typename T>
CObjectPool<T>::~CObjectPool()
{
	ClearAllObject();
}


//
// 오브젝트풀 초기화
// 정해진 크기만큼 풀을 미리 만들어 놓는다
// 풀 사이즈 초과시 assert 에러 발생
//
template<typename T>
void CObjectPool<T>::InitializePool(size_t szPoolSize)
{
	CHECK(0 < szPoolSize)
		<< "Pool size must be positive";

	CHECK(MAX_POOL_SIZE >= szPoolSize)
		<< "Pool size must be less then MAX_POOL_SIZE";

	CSyncronize _locK(&m_lock);
	for (size_t i = 0; i < szPoolSize; i++)
	{
		if (MAX_POOL_SIZE < m_qFreeList.size())
			break;
		AllocObject();
	}
}

template <typename T>
shared_ptr<T> CObjectPool<T>::AquireObject()
{
	CSyncronize _locK(&m_lock);
	if ( m_qFreeList.empty())
	{
		ExtendPool();
	}

	auto obj = m_qFreeList.front();
	m_qFreeList.pop();
	return obj;
}

template <typename T>
void CObjectPool<T>::ReleaseObject(shared_ptr<T> obj)
{
	CSyncronize _locK(&m_lock);
	m_qFreeList.push(obj);
}

template <typename T>
void CObjectPool<T>::AllocObject()
{
	if (MAX_POOL_SIZE < m_qFreeList.size())
		return;

	m_createdSize++;
	shared_ptr<T> pObj = std::make_shared<T>();
	m_qFreeList.push(pObj);
}

template<typename T>
void CObjectPool<T>::ExtendPool()
{
	LOG(INFO) << "Extend POol start";
	for (size_t i = 0; i < m_szChunk; i++)
	{
		if (MAX_POOL_SIZE < m_qFreeList.size())
			break;
		AllocObject();
	}

	LOG(INFO) << "Extend POol end";
}

template <typename T>
void CObjectPool<T>::ClearAllObject()
{
	CSyncronize _locK(&m_lock);
	m_qFreeList = {};
	LOG(INFO) << "ObjectPool ClearALL";
}

template <typename T>
size_t CObjectPool<T>::GetSize()
{
	return m_qFreeList.size();
}