========================================================================
    STATIC LIBRARY : CommonLib Project Overview
========================================================================

AppWizard has created this CommonLib library project for you.

This file contains a summary of what you will find in each of the files that
make up your CommonLib application.


CommonLib.vcxproj
    This is the main project file for VC++ projects generated using an Application Wizard.
    It contains information about the version of Visual C++ that generated the file, and
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

CommonLib.vcxproj.filters
    This is the filters file for VC++ projects generated using an Application Wizard. 
    It contains information about the association between the files in your project 
    and the filters. This association is used in the IDE to show grouping of files with
    similar extensions under a specific node (for e.g. ".cpp" files are associated with the
    "Source Files" filter).



////////////////////////////////////////////////////////////////////////////

Common 라이브러리 파일 정보


Common.h
	프로젝트에서 공용으로 사용 될 헤더파일 정보가 포함되어있는 헤더 파일
	
ObjectPool.h 
	오브젝트풀 클래스

Noncopyable.h
	객체 복사생성자 방지 클래스

ThreadSync.h
	멀티쓰레드 프로그래밍시 동기화 관련 헤더파일
    

/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named CommonLib.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" comments to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
