#pragma once

#include <winsock2.h>
#pragma comment(lib, "ws2_32.lib")

#include <Mswsock.h>
#pragma comment(lib, "Mswsock.lib")

#define GOOGLE_GLOG_DLL_DECL
#define GLOG_NO_ABBREVIATED_SEVERITIES
#include <glog\logging.h>
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

// thread
#include <process.h>

// stl
#include <memory>
#include <vector>
#include <queue>
#include <map>

// Debug
#include <crtdbg.h>

using std::shared_ptr;