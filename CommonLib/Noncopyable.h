#pragma once

/*
 * 객체 복사 방지
 * 컴파일시 에러
 * http://www.boost.org/doc/libs/1_37_0/boost/noncopyable.hpp
 */
class CNoncopyable
{
private:
	CNoncopyable(const CNoncopyable&) = delete;
	const CNoncopyable& operator=(const CNoncopyable&) = delete;
protected:
	CNoncopyable()  {}
	~CNoncopyable() {}
};