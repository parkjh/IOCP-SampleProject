#pragma once
#include "Noncopyable.h"

class CThread :
	public CNoncopyable
{
public:
	CThread();
	virtual ~CThread();

protected:
	HANDLE m_hThread;   // Thread Handle
	UINT32 m_nThreadId; // Thread ID

public:
	BOOL Create();
	BOOL Stop();
	BOOL Suspend();
	BOOL Resume();

	virtual BOOL OnInit();
	virtual BOOL OnRun();
	virtual BOOL OnClose();
};