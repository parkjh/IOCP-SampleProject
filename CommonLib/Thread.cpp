#include "stdafx.h"
#include "Thread.h"

// Thread Callback Function
UINT32 WINAPI ThreadCallback(LPVOID lpParam)
{
	CThread* pThread = (CThread*)lpParam;

	if (pThread->OnRun() == FALSE) // 실행 콜백
		return -1;

	if (pThread->OnClose() == FALSE) // 종료 콜백
		return -1;

	return -1;
}

CThread::CThread()
{
	// parameter init
	m_hThread = NULL;
	m_nThreadId = 0;
}


CThread::~CThread()
{
}

BOOL CThread::Create()
{
	if (m_hThread != NULL)
		return FALSE;

	// 쓰레드 생성
	m_hThread = (HANDLE)_beginthreadex(NULL // 보안 설정 Default
		, 0 // stack size Default
		, ThreadCallback
		, this
		, 0
		, &m_nThreadId);

	if (m_hThread == NULL)
		return FALSE;

	OnInit();

	return TRUE;
}

BOOL CThread::Suspend()
{
	return ::SuspendThread(m_hThread);
}

BOOL CThread::Resume()
{
	return ::ResumeThread(m_hThread);
}


//cabllback Function
BOOL CThread::OnInit()
{
	return TRUE;
}

BOOL CThread::OnRun()
{
	return TRUE;
}

BOOL CThread::OnClose()
{
	return TRUE;
}