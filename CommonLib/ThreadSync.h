#pragma once
#include <Windows.h>
#include "Noncopyable.h"

/* 
 * 쓰레드 동기화 클래스 ( Critical Section )
 */
class CSyncObj : public CNoncopyable
{
private:
	CRITICAL_SECTION m_cs;

public:
	CSyncObj()
	{
		::InitializeCriticalSection(&m_cs);
	}

	explicit CSyncObj(unsigned int spincount)
	{
		::InitializeCriticalSectionAndSpinCount(&m_cs, spincount);
	}

	virtual ~CSyncObj()
	{
		::DeleteCriticalSection(&m_cs);
	}

public:
	BOOL Lock()		{ __try{ ::EnterCriticalSection(&m_cs); } __except (1){ return FALSE; }return TRUE; }
	BOOL Unlock()	{ __try{ ::LeaveCriticalSection(&m_cs); } __except (1){ return FALSE; }return TRUE; }
	BOOL TryEnter()	{ return ::TryEnterCriticalSection(&m_cs) != FALSE; }
};


/*
 * 쓰레드 자동 동기화
 */
class CSyncronize : public CNoncopyable
{
private:
	CSyncObj& m_lockObj;
	BOOL m_isLooked;

public:
	explicit CSyncronize(CSyncObj* lockObj, BOOL isInit = TRUE) : m_lockObj(*lockObj), m_isLooked(FALSE)
	{
		if (isInit) Lock();
	}

	explicit CSyncronize(CSyncObj& lockObj, BOOL isInit = TRUE) : m_lockObj(lockObj), m_isLooked(FALSE)
	{
		if (isInit) Lock();
	}

	~CSyncronize()
	{
		if (m_isLooked) Unlock();
	}

	VOID Lock()
	{
		if (!m_isLooked)
		{
			m_lockObj.Lock();
			m_isLooked = TRUE;
		}
	}

	VOID Unlock()
	{
		if (m_isLooked)
		{
			m_isLooked = FALSE;
			m_lockObj.Unlock();
		}
	}

};