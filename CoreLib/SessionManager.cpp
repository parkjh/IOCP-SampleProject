#include "stdafx.h"
#include "SessionManager.h"

CSessionManager::CSessionManager()
{

}

CSessionManager::~CSessionManager()
{

}

void CSessionManager::Initialize(size_t szPool)
{
	m_sessionPool.InitializePool(szPool);
	LOG(INFO) << "Session Pool inintialize completed, [Prepare Size:" << szPool << "]";
}

shared_ptr<CPacketSession> CSessionManager::AquireSession()
{
	auto pSession = m_sessionPool.AquireObject();
	if (nullptr == pSession)
	{
		return pSession;
	}
	
	CSyncronize _lock(&m_lock);
	m_ConnectionMap.insert(std::make_pair(pSession->GetSeqId(), pSession));
	return pSession;
}

void CSessionManager::ReleaseSession(shared_ptr<CPacketSession> pSession)
{
	ReleaseSession(pSession->GetSeqId());
}

void CSessionManager::ReleaseSession(UINT32 seqId)
{
	CSyncronize _lock(&m_lock);
	auto itor = m_ConnectionMap.find(seqId);

	if (m_ConnectionMap.end() != itor)
	{
		m_sessionPool.ReleaseObject(itor->second);
		m_ConnectionMap.erase(itor);
	}		
	else
		LOG(ERROR) << "Session Release fail, session_map is not exists";	
}

size_t CSessionManager::GetActiveSize()
{
	return m_ConnectionMap.size();
}

size_t CSessionManager::GetFreeSize()
{
	return m_sessionPool.GetSize();
}