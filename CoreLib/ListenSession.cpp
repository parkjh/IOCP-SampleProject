#include "stdafx.h"
#include "ListenSession.h"

CListenSession::CListenSession()
{
	m_pIocpBase = NULL;
}

CListenSession::CListenSession(CIocpBase* pIocp) : m_pIocpBase(pIocp)
{

}

CListenSession::~CListenSession()
{
}

BOOL CListenSession::Start()
{
	LOG(INFO) << "Listen Session Prepare Startup";

	CSyncronize _lock(&m_cs);
	m_sessionMgr.Initialize(MAX_SESSION_SIZE);	
	return TRUE;
}

VOID CListenSession::Close()
{
	CSyncronize _lock(&m_cs);	
	CloseSocket();
	for (auto& _thread : m_vtAcceptThread)
		_thread.join();	

	m_vtAcceptThread.clear();
}

BOOL CListenSession::Listen(CONST LPSTR addr,  USHORT port)
{
	CSyncronize _lock(&m_cs);

	if (0 >= port)
		return FALSE;

	if (m_socket)
		return FALSE;

	// Bind
	m_socket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);	
	if (INVALID_SOCKET == m_socket)
		return FALSE;

	m_addrLocal.sin_family = AF_INET;
	m_addrLocal.sin_port = htons(port);
	m_addrLocal.sin_addr.S_un.S_addr = (addr ? inet_addr(addr) : htonl(INADDR_ANY));

	if (::bind(m_socket, (LPSOCKADDR)&m_addrLocal, sizeof(m_addrLocal)) == SOCKET_ERROR)
	{
		Close();
		return FALSE;
	}

	// Listen
	if (::listen(m_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		Close();
		return FALSE;
	}

	LOG(INFO) << "Start Listen Socket ..."
	<< "\n\t\t\t\t\t\t  - Host : " << ((addr == nullptr) ? "127.0.0.1" : addr)
	<< "\n\t\t\t\t\t\t  - Port : " << port;

	return TRUE;
}

VOID CListenSession::Accept()
{
	CSyncronize _lock(&m_cs);
	m_vtAcceptThread.push_back(std::thread([](LPVOID lpParam){
		CListenSession* pThis = (CListenSession*)lpParam;
		pThis->AcceptThreadFunc();
	}, this));

}

VOID CListenSession::AcceptThreadFunc()
{
	LOG(INFO) << "Accept Start";

	while (true)
	{
		SOCKET clntSock = INVALID_SOCKET;
		SOCKADDR_IN clntAddr;
		int addrlen = sizeof(clntAddr);

		clntSock = ::accept(m_socket, (sockaddr*)&clntAddr, &addrlen);
		if (INVALID_SOCKET == clntSock)
		{
			HRESULT hresult = WSAGetLastError();
			if ( !(WSA_IO_PENDING == hresult ||
				   WSAEWOULDBLOCK == hresult ||
				   WSAEISCONN == hresult))
			{
				break;
			}
		}

		auto pSession = m_sessionMgr.AquireSession();
		pSession->InitializeSession(clntSock);
		m_pIocpBase->OnConnected(pSession.get());
	}	
}

VOID CListenSession::AcceptEx()
{

}

VOID CListenSession::ReleaseSession(UINT64 seqId)
{
	m_sessionMgr.ReleaseSession(seqId);
}

BOOL CListenSession::OnRecv(DWORD& dwTrans)
{
	return TRUE;
}

BOOL CListenSession::OnSend(DWORD& dwTrans)
{
	return TRUE;
}