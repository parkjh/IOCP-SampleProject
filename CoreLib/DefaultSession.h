#pragma once
#include "IocpDefine.h"
#include "IocpBuffer.h"
#include "PacketHeader.h"
#include "../CommonLib/ThreadSync.h"
#include "../CommonLib/Noncopyable.h"

typedef shared_ptr<PacketBuffer> ptrPacketBuffer;
typedef shared_ptr<CIocpBuffer> ptrIocpBuffer;

//
// 扁夯 技记 Class
// 匙飘况农 LowLevel包访 贸府
//
class CDefaultSession: public CNoncopyable
{
public:
	CDefaultSession();
	virtual ~CDefaultSession();

protected:
	CSyncObj m_cs;
	
	UINT32 m_nSeqId;

	SOCKET m_socket;
	SOCKADDR_IN m_addrLocal;
	SOCKADDR_IN m_addrRemote;

	BOOL m_isClosing;
	
	ULONG m_nRecvSeq;
	ULONG m_nSendSeq;
	ULONG m_nSendRef;

	DWORD m_dwRecvBytes;
	DWORD m_dwSendRemainBytes;

	OverlappedEx m_recvOverlapped;
	OverlappedEx m_sendOverlapped;

	BYTE m_recvBuffer[MAX_BUFFER_SIZE];	
	std::queue<ptrIocpBuffer> m_qSendBuffer;

public:
	UINT32 GetSeqId() { return m_nSeqId; }
	SOCKET GetSocket();	
	BOOL   CloseSocket();

	BOOL InitializeSession(SOCKET sock);
	BOOL RecycleSocket(SOCKET sock);

	BOOL PostRecvIocp();
	BOOL PostSendIocp(BYTE* pData, DWORD dwDataLength);

	virtual BOOL OnRecv(DWORD& dwByteTransfered) PURE;
	virtual BOOL OnSend(DWORD& dwByteTransfered) PURE;
};