#include "stdafx.h"
#include "IocpServer.h"
#include "PacketSession.h"
#include "Configure.h"
#include "ObjectPoolManager.h"

CIocpServer::CIocpServer()
	:m_pListenSession(std::make_shared<CListenSession>(this)),
	 m_pConf(std::make_shared<CConfigure>())
{
}

CIocpServer::~CIocpServer()
{
	LOG(INFO) << "서버 종료";
	CIocpServer::Close();
}

//
// 서버 초기화
// 초기화 세팅시 실패할 경우 프로그램 중단
//
BOOL CIocpServer::Initialize()
{
	LOG(INFO) << "IOCP Server Initialize Start ...";

	// 설정 초기화
	m_pConf->InitailizeConf();

	// Winsock DLL 로드
	WSADATA wsaData;
	CHECK(::WSAStartup(MAKEWORD(2, 2), &wsaData) == 0)
		<< "Code:" << ::WSAGetLastError();

	CHECK(wsaData.wVersion == MAKEWORD(2, 2))
		<< "Code:" << ::WSAGetLastError();
	LOG(INFO) << "WinSock2 DLL Load Completed";

	return TRUE;
}


VOID CIocpServer::CreateLogicThread(void func())
{
	std::thread logicThread = std::thread(func);
	LOG(INFO) << "thread_id:" << logicThread.get_id();
	m_vtLogicThread.push_back(std::move(logicThread));
}

VOID CIocpServer::CloseAllLogicThread()
{
	for (auto& t : m_vtLogicThread)
	{
		t.join();
	}

	m_vtLogicThread.clear();
}

VOID CIocpServer::Start(CONST LPSTR addr, USHORT port, DWORD dwThreadCnt, DWORD dwMaxSessionCnt)
{
	LOG(INFO) << "================================";
	LOG(INFO) << "IOCP Server StartUp";
	LOG(INFO) << "================================";

	if (!Initialize())
		return;

	// Iocp 핸들 생성
	CHECK(CIocpBase::Create(dwThreadCnt))
		<< "Iocp Handle Create Fail";

	// object pool 생성
	CObjectPoolManager::GetInstance().InitializePools();

	// TCP Server Bind -> Listen -> Accept
	CHECK(m_pListenSession->Start())
		<< "Listen Initialize Fail";
	CHECK(m_pListenSession->Listen(addr, port))
		<< "Listen Fail";
	m_pListenSession->Accept();
}

VOID CIocpServer::Close()
{	
	// Listen Close	
	if (m_pListenSession)
	{
		m_pListenSession->Close();
		m_pListenSession.reset();

		LOG(INFO) << "Listen Session is Closed";
	}

	CIocpBase::Close();
	CloseAllLogicThread();

	CObjectPoolManager::GetInstance().ClearAll();
	LOG(INFO) << "Object Pool Release";

	::WSACleanup();

	LOG(INFO) << "Iocp Server Close Completed";
	
	m_pConf->ReleaseAllConf(); // 설정 해제
}



VOID CIocpServer::OnConnected(LPVOID lpParam)
{
	DLOG(INFO) << "Client Connected";

	auto pSession = reinterpret_cast<CPacketSession*>(lpParam);
	// Iocp 핸들에 등록	
	if (!CIocpBase::RegisterSocket(pSession->GetSocket(), (ULONG_PTR)pSession))
	{
		LOG(ERROR) << "IOCP Register Socket Failed";
	}
	// Io Recv 준비
	pSession->PostRecvIocp();
}

VOID  CIocpServer::OnDisconnected(LPVOID lpParam)
{
	auto pSession = reinterpret_cast<CPacketSession*>(lpParam);
	pSession->ClearSession();
	m_pListenSession->ReleaseSession(pSession->GetSeqId());

	DLOG(INFO) << "On Disconnected";
}

BOOL CIocpServer::OnRecv(LPVOID lpParam, DWORD dwByteTransfered)
{
	//DLOG(INFO) << "On Recv Callback";

	auto pSession = reinterpret_cast<CPacketSession*>(lpParam);

	if (!pSession->OnRecv(dwByteTransfered))
		return FALSE;

	if (!pSession->PostRecvIocp())
		return FALSE;
	
	return TRUE;	
}

BOOL CIocpServer::OnSend(LPVOID lpParam, DWORD dwByteTransfered)
{	
	//LOG(INFO) << "OnSend Callback";
	auto pSession = reinterpret_cast<CPacketSession*>(lpParam);
	pSession->OnSend(dwByteTransfered);

	return TRUE;
}
