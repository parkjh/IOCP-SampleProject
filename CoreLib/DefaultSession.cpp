#include "stdafx.h"
#include "DefaultSession.h"
#include <atomic>


CDefaultSession::CDefaultSession()
{
	// 세션 마다 sequence Id 발급
	static std::atomic<UINT32> m_nSeed;
	m_nSeqId = ++m_nSeed;
	
	m_socket = NULL;
	memset(&m_recvBuffer, 0, sizeof(m_recvBuffer));
	memset(&m_recvOverlapped, 0, sizeof(m_recvOverlapped));
	memset(&m_sendOverlapped, 0, sizeof(m_sendOverlapped));

	m_recvOverlapped.eOverlapperdType = OverLappedType::IO_RECV;
	m_sendOverlapped.eOverlapperdType = OverLappedType::IO_SEND;

	m_dwRecvBytes = 0;
	m_dwSendRemainBytes = 0;
}

CDefaultSession::~CDefaultSession()
{
}

BOOL CDefaultSession::InitializeSession(SOCKET sock)
{
	m_socket = sock;

	memset(&m_recvBuffer, 0, sizeof(m_recvBuffer));
	memset(&m_recvOverlapped, 0, sizeof(m_recvOverlapped));
	memset(&m_sendOverlapped, 0, sizeof(m_sendOverlapped));

	m_recvOverlapped.eOverlapperdType = OverLappedType::IO_RECV;
	m_sendOverlapped.eOverlapperdType = OverLappedType::IO_SEND;

	m_dwRecvBytes = 0;
	m_dwSendRemainBytes = 0;

	return TRUE;
}

SOCKET CDefaultSession::GetSocket()
{
	return m_socket;
}

BOOL CDefaultSession::CloseSocket()
{
	if (GetSocket() == NULL)
		return FALSE;

	closesocket(GetSocket());
	m_socket = NULL;
}

BOOL CDefaultSession::PostRecvIocp()
{	
	if (INVALID_SOCKET == GetSocket())
		return FALSE;

	WSABUF wsaBuf;
	DWORD  dwNumberOfBytesRecvd = 0;
	DWORD  dwRecvLength = 0;
	DWORD  dwRecvFlag = 0;

	// Recv
	wsaBuf.buf = (CHAR*)(m_recvBuffer + m_dwRecvBytes);
	wsaBuf.len = MAX_BUFFER_SIZE - m_dwRecvBytes;

	INT32 ret = ::WSARecv(m_socket, 
		&wsaBuf, 
		1, 
		&dwNumberOfBytesRecvd,
		&dwRecvFlag, 
		(LPOVERLAPPED)&m_recvOverlapped,
 		NULL);

	if (SOCKET_ERROR == ret &&
		WSA_IO_PENDING != WSAGetLastError() &&
		WSAEWOULDBLOCK != WSAGetLastError())
	{
		// 세션 반환
		LOG(ERROR) << "WSARecv Error, code:" << WSAGetLastError();
		return FALSE;
	}

	return TRUE;
}

BOOL CDefaultSession::PostSendIocp(BYTE* pData, DWORD dwDataLength)
{
	if (INVALID_SOCKET == m_socket)
		return FALSE;	
	if (nullptr == pData || dwDataLength <= 0)
		return FALSE;

	WSABUF wsaBuf;
	DWORD dwBufferCount;
	DWORD  dwNumberOfBytesSent = 0;

	wsaBuf.buf = (CHAR*)pData;
	wsaBuf.len = dwDataLength;

	INT32 ret = ::WSASend(m_socket, 
		&wsaBuf, 
		1, 
		&dwNumberOfBytesSent, 
		0, 
		(LPOVERLAPPED)&m_sendOverlapped, 
		NULL);

	if (SOCKET_ERROR == ret &&
		WSA_IO_PENDING != WSAGetLastError() &&
		WSAEWOULDBLOCK != WSAGetLastError())
	{
		// 세션 반환
		LOG(ERROR) << "wsasend error: " << WSAGetLastError();
		return FALSE;
	}

	return TRUE;
}