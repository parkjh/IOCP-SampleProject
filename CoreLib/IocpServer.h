#pragma once
#include "IocpBase.h"
#include "ListenSession.h"
#include "Configure.h"

class CIocpServer : public CIocpBase
{
public:
	CIocpServer();
	virtual ~CIocpServer();

private:
	std::shared_ptr<CConfigure> m_pConf;
	std::shared_ptr<CListenSession> m_pListenSession;
	std::vector<std::thread> m_vtLogicThread;
	
public:
	BOOL Initialize();
	VOID Start(CONST LPSTR addr, USHORT port, DWORD dwThradCnt, DWORD dwMaxSessionCnt);
	VOID Close();
	VOID CreateLogicThread(void func());
	VOID CloseAllLogicThread();

protected:
	VOID OnConnected(LPVOID pObj) override;
	VOID OnDisconnected(LPVOID pObj) override;
	BOOL OnRecv(LPVOID pObj, DWORD dwByteTransfered) override;
	BOOL OnSend(LPVOID pObj, DWORD dwByteTransfered) override;
};