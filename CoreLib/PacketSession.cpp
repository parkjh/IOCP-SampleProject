#include "stdafx.h"
#include "PacketSession.h"
#include "PacketHeader.h"
#include "ProtocolManager.h"

CPacketSession::CPacketSession()
{
}

CPacketSession::CPacketSession(SOCKET sock)
{
	if (!m_socket)
		m_socket = sock;
}

CPacketSession::~CPacketSession()
{

}

BOOL CPacketSession::SendPacket(PacketHeader* hdr)
{
	return CPacketSession::SendPacket(
		(char*)hdr, 
		hdr->GetSize());
}

BOOL CPacketSession::SendPacket(const char* pData, DWORD dwLength)
{
	CSyncronize _lock(&m_cs);
	
	if (nullptr == pData || 0 == dwLength)
		return FALSE;
	
	shared_ptr<CIocpBuffer> pIocpBuffer = std::make_shared<CIocpBuffer>();
	pIocpBuffer->m_szBufferSize = dwLength;
	memcpy(pIocpBuffer->m_pBuffer.get(), pData, dwLength);

	// 대기열이 없는 경우 바로 메세지 전송
	if (m_qSendBuffer.empty())
	{
		// 현재 버퍼에 보낼 크기 누적
		m_dwSendRemainBytes += dwLength;
		m_qSendBuffer.push(pIocpBuffer);
		PostSendIocp((BYTE*)pIocpBuffer->m_pBuffer.get(), dwLength);
		return TRUE;
	}
	// 전송 중인 메세지가 있는 경우 큐에 넣고 대기
	else
	{
		m_qSendBuffer.push(pIocpBuffer);
		return TRUE;
	}
}

BOOL CPacketSession::OnRecv(DWORD& dwByteTransfered)
{
	// 연결이 끊어진 경우
	if (0 == dwByteTransfered)
	{
		LOG(INFO) << "Socket Recv: dwByteTransfered is 0";
		return FALSE;
	}
	
	m_dwRecvBytes += dwByteTransfered;
	
	// 받은 데이터의 크기가 헤더보다 커야지 파싱이 가능
	while (m_dwRecvBytes >= sizeof(PacketHeader))
	{
		PacketHeader* header = (PacketHeader*)m_recvBuffer;	
		int nPacketSize = header->GetSize();

		//LOG(INFO) << "받은 데이터크기:" << header->size;
	
		// 버퍼 사이즈보다 클 경우 에러
		if (nPacketSize > MAX_BUFFER_SIZE)
		{
			// 잘못된 패킷일 경우 무시
			m_dwRecvBytes = 0;
			LOG(ERROR) << "buffer is not enoungh, size:" << nPacketSize;
			return FALSE;
		}

		// 패킷이 완료된 경우 Queue에 적재 후 버퍼 이동
		if (m_dwRecvBytes >= nPacketSize)
		{
			// process handler
			CProtocolManager::GetInstance().ProcessHandle((PacketHeader*)m_recvBuffer, this);
			if ((m_dwRecvBytes - nPacketSize) > 0)
			{
				memmove(m_recvBuffer, m_recvBuffer + nPacketSize, m_dwRecvBytes - nPacketSize);
			}

			m_dwRecvBytes -= nPacketSize;
			continue;;
		}

		break;
	}
		
	return TRUE;
}

BOOL CPacketSession::OnSend(DWORD& dwByteTransfered)
{
	CSyncronize _lock(&m_cs);

	if (INVALID_SOCKET == m_socket)
		return FALSE;

	//// 전송된 길이가 더 많은 경우 에러 
	if (dwByteTransfered > m_dwSendRemainBytes)
	{
		return FALSE;
	}

	m_dwSendRemainBytes -= dwByteTransfered;
	//
	// 패킷 전송이 완료된 경우
	if (0 == m_dwSendRemainBytes)
	{
		DLOG(INFO) << "Send Completed, send queue pop packet";
		m_qSendBuffer.pop();
	}	
	// Send가 남아 있는 경우 처리
	else if (0 < m_dwSendRemainBytes)
	{
		auto pIocpBuffer = m_qSendBuffer.front();
		BYTE* pData = (BYTE*)pIocpBuffer->m_pBuffer.get();
		PostSendIocp((pData + dwByteTransfered), m_dwSendRemainBytes);

		return TRUE;
	}
	
	// 큐에 보낼 메세지가 있을 경우 다음 메세지 전송
	if (!m_qSendBuffer.empty())
	{
		auto pIocpBuffer = m_qSendBuffer.front();
		BYTE* pData = (BYTE*)pIocpBuffer->m_pBuffer.get();
		m_dwSendRemainBytes += pIocpBuffer->GetSize();
		PostSendIocp(pData, m_dwSendRemainBytes);		
	}

	return TRUE;
}

BOOL CPacketSession::ClearSession()
{
	//CSyncronize _lock(&m_cs);
	CDefaultSession::InitializeSession(NULL);
	m_qSendBuffer = {}; // c++ 11 
	return TRUE;
}