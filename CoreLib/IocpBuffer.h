#pragma once
#include "PacketHeader.h"

#define MAX_BUFFER_SIZE 4096

//
// Packet �⺻��
//
struct PacketBuffer : PacketHeader
{
	BYTE buf[MAX_BUFFER_SIZE - sizeof(PacketHeader)];

	PacketBuffer()
	{
		ZeroMemory(buf, 0);
	}
};

class CIocpBuffer
{
public:
	shared_ptr<PacketBuffer> m_pBuffer;
	DWORD m_szBufferSize = 0;

	CIocpBuffer();
	virtual ~CIocpBuffer();

	size_t GetSize();
};