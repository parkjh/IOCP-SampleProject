#pragma once
#include "stdafx.h"

//
// IOCP 관련 정보 헤더
// 오버렙드 확장 정보 및 enum 정의 값
//
enum class OverLappedType
{
	IO_NULL = 0,
	IO_ACCEPT,
	IO_RECV,
	IO_SEND
};

struct OverlappedEx: OVERLAPPED
{	
	OverLappedType eOverlapperdType;
};