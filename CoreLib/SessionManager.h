#pragma once
#include "../CommonLib/ObjectPool.h"
#include "../CommonLib/ThreadSync.h"
#include "../CommonLib/Noncopyable.h"
#include "PacketSession.h"

//
// 네트워크 세션 관리 클래스
// 해당 되는 서버 소켓의 세션풀을 관리
//
typedef std::map<UINT32, shared_ptr<CPacketSession>> MapSession;
class CSessionManager: public CNoncopyable
{
private:
	// Lock object
	CSyncObj m_lock;

	// 대기중인 풀
	// 세션풀의 경우 서버마다 세션이 생기므로 별도로 관리하게 구성함
	CObjectPool<CPacketSession> m_sessionPool;

	// 접속 중인 유저 리스트
	MapSession m_ConnectionMap;
public:
	CSessionManager();
	virtual ~CSessionManager();

	void Initialize(size_t szPool);
	shared_ptr<CPacketSession> AquireSession();
	void ReleaseSession(shared_ptr<CPacketSession> pSession);
	void ReleaseSession(UINT32 seqId);

	size_t GetActiveSize();
	size_t GetFreeSize();
};

