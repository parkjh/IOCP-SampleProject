#include "stdafx.h"
#include "IocpBase.h"


CIocpBase::CIocpBase()
{
	m_hIocp = NULL;
	m_dwNumberOfConcurrentThreads = 0;
}

CIocpBase::~CIocpBase()
{
}

//
// Iocp 핸들 생성 및 쓰레드 생성
// WorkerThread 수는 0일 경우 process * 2 + 1 만큼 생성
//
BOOL CIocpBase::Create(DWORD dwNumberOfConcurrentThreads)
{
	// 에러 처리 생략 
	// case: 핸들이 이미 존재 하는 경우 처리

	// 서버 시작시 상태값을 변경
	m_bShutdown = false;

	// 핸들 생성
	m_hIocp = ::CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, m_dwNumberOfConcurrentThreads);

	if (IsValidHandle() == FALSE)
		return FALSE;

	// 쓰레드 파라미터가 0일 경우 (cpu * 2) + 1 만큼 쓰레드 생성
	if (0 == dwNumberOfConcurrentThreads)
	{
		SYSTEM_INFO si;
		GetSystemInfo(&si);
		m_dwNumberOfConcurrentThreads = si.dwNumberOfProcessors * 2 + 1;
	}
	else
	{
		m_dwNumberOfConcurrentThreads = dwNumberOfConcurrentThreads;
	}


	m_vtWorkerThread.reserve(m_dwNumberOfConcurrentThreads);

	//// Worker 쓰레드 생성
	for (DWORD i = 0; i < m_dwNumberOfConcurrentThreads; i++)
	{
		// Worker 쓰레드 생성 후 벡터에 푸시
		m_vtWorkerThread.push_back(std::thread([this](){
			WorkerThreadFunc();
		}));
	}
	return TRUE;
}

VOID CIocpBase::Close()
{
	CSyncronize _lock(this);

	// 서버 종료시 플래그 변경
	m_bShutdown = true;

	// 쓰레드 종료 메세지를 workerThread 개수만큼 보내서 쓰레드를 종료시킴
	for (auto& worker : m_vtWorkerThread)
		::PostQueuedCompletionStatus(m_hIocp, 0, NULL, NULL);
	
	// 해당 쓰레드가 종료될때까지 대기
	for (auto& worker : m_vtWorkerThread)
		worker.join();

	m_vtWorkerThread.clear();

	// IOCP 핸들 종료
	if (IsValidHandle())
		::CloseHandle(m_hIocp);
}


//
// IOCP 핸들에 소켓 등록
//
BOOL CIocpBase::RegisterSocket(SOCKET socket, ULONG_PTR pCompletionKey)
{
	if (socket == NULL || pCompletionKey == NULL)
		return FALSE;

	m_hIocp = ::CreateIoCompletionPort((HANDLE)socket, GetHandle(), pCompletionKey, 0);

	return IsValidHandle();
}


//
// WorkerThread Func
//
BOOL CIocpBase::WorkerThreadFunc()
{
	BOOL bRet = FALSE;
	DWORD dwLastError = 0;
	DWORD dwNumberOfByteTransfered = 0;
	ULONG_PTR pCompletionKey = NULL;
	LPOVERLAPPED pOverlapped = nullptr;
	OverlappedEx* pOverlappedEx = NULL;
	
	while (!m_bShutdown)
	{
		bRet = ::GetQueuedCompletionStatus(
			GetHandle(),
			&dwNumberOfByteTransfered,
			&pCompletionKey,
			&pOverlapped,
			INFINITE);

		pOverlappedEx = (OverlappedEx*)pOverlapped;

		// PostQueue로 NULL 보낼시 쓰레드 종료처리
		if (pCompletionKey == NULL)
		{
			LOG(INFO) << "IOCP Worker Thread Exit, pCompletedKey == NULL";
			return FALSE;
		}


		// 정상 종료: isSuccess: false, recvData: 0
		// 비정상종료: isSuccess: true, recvData: 0

		// iocp 통보가 정상적이지 않을 경우 처리
		if (!bRet)
		{
			dwLastError = ::GetLastError();

			// code: 64
			// AcceptEx호출시 backlog에 있던 소켓이 accept가 되지전에 접속을 끊는 경우 발생
			if (ERROR_NETNAME_DELETED == dwLastError)
			{
				// 해당 소켓이 listen소켓일 경우 accept수행
				if (pOverlappedEx->eOverlapperdType == OverLappedType::IO_ACCEPT)
				{
					OnConnected((LPVOID)pCompletionKey);
				}
				else
				{
					OnDisconnected((LPVOID)pCompletionKey);
				}

				continue;
			}

			if (WAIT_TIMEOUT == dwLastError)
				continue;
			if (nullptr != pOverlapped)
				continue;			

			LOG(ERROR) << "IOCP Worker Thread Exit, GetQueuedCompletionStatus ERROR, Code:" << dwLastError;
			break;
		}

		//
		// IOCP 통보가 완료된 경우 처리
		// bRet == TRUE
		// 
		switch (pOverlappedEx->eOverlapperdType)
		{
		case OverLappedType::IO_ACCEPT:
			// TODO
			// ACCEPT overlapped의 경우에 socket정보를 넘겨주어야 됨!!!
			OnConnected((LPVOID)pCompletionKey);
			break;
		case OverLappedType::IO_RECV:
			if (0 == dwNumberOfByteTransfered ||
				FALSE == OnRecv((LPVOID)pCompletionKey, dwNumberOfByteTransfered))
			{
				OnDisconnected((LPVOID)pCompletionKey);
			}			
			break;
		case OverLappedType::IO_SEND:
			OnSend((LPVOID)pCompletionKey, dwNumberOfByteTransfered);
			break;

		case OverLappedType::IO_NULL:
		default:
			LOG(ERROR) << "Overlapped Type is IO_NULL";
			break;
		}
	}

	return TRUE;
}