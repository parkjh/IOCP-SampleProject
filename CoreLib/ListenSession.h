#pragma once
#include "IocpBase.h"
#include "SessionManager.h"

/*
 * Iocp Server Listen Session
 */

#define MAX_SESSION_SIZE 3000

class CListenSession : public CDefaultSession
{
public:
	CListenSession();
	explicit CListenSession(CIocpBase* pIocp);
	virtual ~CListenSession();

private:
	std::vector<std::thread> m_vtAcceptThread;
	OverlappedEx* m_OverlappedAccept;
	CIocpBase* m_pIocpBase;
	CSessionManager m_sessionMgr;
public:
	BOOL Start();
	VOID Close();
	BOOL Listen(CONST LPSTR addr, USHORT port);
	
	VOID Accept();
	VOID AcceptThreadFunc();
	VOID AcceptEx();

	VOID ReleaseSession(UINT64 seqId);
	BOOL OnRecv(DWORD& dwTrans) override;
	BOOL OnSend(DWORD& dwTrans) override;
};