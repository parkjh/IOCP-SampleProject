#include "stdafx.h"
#include "ProtocolManager.h"


CProtocolManager::CProtocolManager()
{
	Inintialize();
}


CProtocolManager::~CProtocolManager()
{
}

VOID CProtocolManager::Inintialize()
{
	// Test Protocol
	m_protocolMap[1] = [](PacketHeader* pHeader, CPacketSession* pSession){
		pSession->SendPacket("hello", 5);
	};
}

VOID CProtocolManager::ProcessHandle(PacketHeader* pHeader, CPacketSession* pSession)
{
	if (nullptr == pHeader)
	{
		LOG(ERROR) << "header is NULL";
		pSession->CloseSocket();
		return;
	}

	if (nullptr == pSession)
	{
		LOG(ERROR) << "pSession is NULL";
		pSession->CloseSocket();
		return;
	}

	auto itor = m_protocolMap.find(pHeader->cmd);
	if (itor == m_protocolMap.end())
	{
		LOG(ERROR) << "cmd is not exists, cmd:" << pHeader->cmd;
		pSession->CloseSocket();
		return;
	}

	itor->second(pHeader, pSession);
	return;
}