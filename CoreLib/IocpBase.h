#pragma once
#include <thread>
#include "IocpDefine.h"
#include "../CommonLib/ThreadSync.h"

//
// IOCP Class
//
class CIocpBase : public CSyncObj
{
private:
	HANDLE m_hIocp;
	DWORD m_dwNumberOfConcurrentThreads;

	std::vector<std::thread> m_vtWorkerThread;

	BOOL m_bShutdown = false;

public:
	CIocpBase();
	virtual ~CIocpBase();

public:
	// Iocp Handle create
	BOOL Create(DWORD dwNumberOfConcurrentThreads = 0);
	VOID Close();
	
	BOOL RegisterSocket(SOCKET socket, ULONG_PTR pCompletionKey);

	// Inline function
	BOOL IsValidHandle() { return (m_hIocp == NULL ? FALSE : TRUE); }
	HANDLE GetHandle() { return m_hIocp; }

private:
	BOOL WorkerThreadFunc();

protected:	
	virtual BOOL OnRecv(LPVOID pObj, DWORD dwByteTransfered) PURE;
	virtual BOOL OnSend(LPVOID pObj, DWORD dwByteTransfered) PURE;
	
public:
	// Accept�� ȣ��
	virtual VOID OnConnected(LPVOID pObj) PURE;
	virtual VOID OnDisconnected(LPVOID obj)	PURE;
};