#include "stdafx.h"
#include "IocpBuffer.h"
#include "ObjectPoolManager.h"

CIocpBuffer::CIocpBuffer()
	: m_szBufferSize(0),
	  m_pBuffer(CObjectPoolManager::GetInstance().Aquire<PacketBuffer>())
{
	if (m_pBuffer)
		memset(m_pBuffer.get(), 0, sizeof(PacketBuffer));
}


CIocpBuffer::~CIocpBuffer()
{
	if (m_pBuffer)
		CObjectPoolManager::GetInstance().Release(m_pBuffer);
}

size_t CIocpBuffer::GetSize()
{
	return m_szBufferSize;
}