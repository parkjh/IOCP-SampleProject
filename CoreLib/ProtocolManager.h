#pragma once
#include <functional>
#include "../CommonLib/TSingleton.h"
#include "../CommonLib/ThreadSync.h"
#include "PacketSession.h"

//
// 프로토콜 핸들링 Class
//
typedef void(*ProcessFunc)(PacketHeader* header, CPacketSession* pSession);
class CProtocolManager : public TSingleton<CProtocolManager>
{
public:
	CProtocolManager();
	virtual ~CProtocolManager();

private:
	CSyncObj m_lock;
	std::map<int, std::function < void(PacketHeader*, CPacketSession*)>> m_protocolMap;

public:
	VOID Inintialize();
	VOID ProcessHandle(PacketHeader* pHeader, CPacketSession* pSession);
};