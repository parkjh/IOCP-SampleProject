#include "stdafx.h"
#include "Configure.h"

#include <string>
#include <boost\property_tree\json_parser.hpp>
#include <boost\property_tree\ptree.hpp>
#include <boost\foreach.hpp>

CConfigure::CConfigure()
{
	// 로깅은 자동으로 설정 
	InitializeLogger();
}

CConfigure::~CConfigure()
{
}

void CConfigure::InitailizeConf()
{	
	LOG(INFO) << "Prepare Configure ...";
}

void CConfigure::ReleaseAllConf()
{
	LOG(INFO) << "Release Configure ...";
	ReleaseLogger();
}

void CConfigure::InitializeLogger()
{
	// Glog 초기화는 한번만 동작하도록 함
	static std::once_flag initflag;
	std::call_once(initflag, [](){
		// Glog Init
		google::InitGoogleLogging("IocpServer");
#ifdef _DEBUG
		_putenv_s("GLOG_logbufsecs", "0");
		google::LogToStderr();
		FLAGS_colorlogtostderr = true;
#endif
		FLAGS_log_prefix;

		// 로그 파일 이름 및 디렉토리 설정
		google::SetLogDestination(google::GLOG_INFO, "./info.log");
		google::SetLogDestination(google::GLOG_WARNING, "./warning.log");
		google::SetLogDestination(google::GLOG_ERROR, "./error.log");

		/*for (auto& path : google::GetLoggingDirectories())
		{
		LOG(INFO) << "Log File path: " << path;
		}*/

		LOG(INFO) << "Logging Library(Google-glog) Initialize Completed!";
	});
}

void CConfigure::ReleaseLogger()
{
	LOG(INFO) << "Shutdown Logging";
	google::ShutdownGoogleLogging();
}