#pragma once
#include "../CommonLib/TSingleton.h"
#include <mutex>

class CConfigure: public CNoncopyable
{
public:
	CConfigure();
	virtual ~CConfigure();

	void InitailizeConf();
	void ReleaseAllConf();
private:
	void InitializeLogger();
	void ReleaseLogger();
};

