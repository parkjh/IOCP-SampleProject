#pragma once

#pragma pack(push, 1)
struct PacketHeader
{
	unsigned int size;
	unsigned int cmd;
	bool cryted;
	int crc;

	unsigned int GetCommand() const
	{
		return cmd;
	}

	unsigned int GetSize() const
	{
		return size;
	}
};

struct TestProtocl : public PacketHeader
{
	WCHAR msg[100];
	TestProtocl()
	{
		cmd = 1;
	}
};

struct  PACKET_BROADCAST: public PacketHeader
{
	WCHAR msg[256];
};

#pragma pack(pop, 1)