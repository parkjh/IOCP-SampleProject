#pragma once
#include "DefaultSession.h"

class CPacketSession;
typedef std::shared_ptr<CPacketSession> ptrSession;

class CPacketSession: public CDefaultSession
{
public:
	CPacketSession();
	CPacketSession(SOCKET sock);
	virtual ~CPacketSession();

	BOOL ClearSession();
	BOOL SendPacket(PacketHeader* hdr);
	BOOL SendPacket(const char* pData, DWORD dwLength);

	BOOL OnRecv(DWORD& dwByteTransfered) override;
	BOOL OnSend(DWORD& dwByteTransfered) override;
};