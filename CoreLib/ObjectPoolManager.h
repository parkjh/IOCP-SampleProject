#pragma once
#include <functional>
#include <typeindex>
#include <typeinfo>
#include <boost\any.hpp>

#include "../CommonLib/TSingleton.h"
#include "../CommonLib/ThreadSync.h"
#include "../CommonLib/ObjectPool.h"


#define TypeIdx(Type) std::type_index(typeid(Type))

class CObjectPoolManager: public TSingleton<CObjectPoolManager>
{
	typedef std::type_index typeIdx;
	typedef std::map < typeIdx, boost::any > PoolContainer;
	
private:
	// Pool Container
	PoolContainer m_poolContainer;
	PoolContainer::iterator itor;
	CSyncObj m_cs;

public:
	void InitializePools();

	CObjectPoolManager();
	virtual ~CObjectPoolManager();	


	//
	// 해당되는 타입에 대해서 오브젝트 풀 생성
	// - ObjectPool은 스마트 포인터로 관리해주고 있음
	// - 이미 풀이 존재할 경우 Assert 에러 발생
	//
	template<typename T>
	void CreatePool(size_t szPoolSize) {
		CSyncronize _lock(&m_cs);
		itor = m_poolContainer.find(TypeIdx(T));
	
		CHECK(m_poolContainer.end() == itor)
			<< "ObjectPool is Exists,  type name :" << typeid(T).name();

		auto pPoolObj = std::make_shared<CObjectPool<T>>();
		pPoolObj->InitializePool(szPoolSize);				
		m_poolContainer[TypeIdx(T)] = pPoolObj;
	}

	void log()
	{

	}

	// 
	// 풀 할당
	//  
	template<typename T>
	shared_ptr<T> Aquire()
	{
		DLOG(INFO) << "AquireBuffer[" << typeid(T).name() << "]";

		CSyncronize _lock(&m_cs);
		itor = m_poolContainer.find(TypeIdx(T));

		if (m_poolContainer.end() != itor)
		{
			auto pPool = boost::any_cast<shared_ptr<CObjectPool<T>>>(itor->second);
			return pPool->AquireObject();
		}

		return NULL;
	}

	//
	// 오브젝트 반환
	// 반환 타입은 반드시 스마트포인터로 반환
	//
	template<typename T>
	void Release(shared_ptr<T> tObj)
	{
		DLOG(INFO) << "ReleaseBuffer[" << typeid(T).name() << "]";

		CSyncronize _lock(&m_cs);

		itor = m_poolContainer.find(TypeIdx(T));
		if (m_poolContainer.end() != itor)
		{
			auto pPool = boost::any_cast<shared_ptr<CObjectPool<T>>>(itor->second);
			pPool->ReleaseObject(tObj);
		}
	}

	void ClearAll()
	{
		CSyncronize _lock(&m_cs);
		m_poolContainer.clear();
	}
};